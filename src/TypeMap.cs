﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace VS.Dapper.SqlMapper
{
    public interface IMembersTypeMap
    {
        Dictionary<string, global::Dapper.SqlMapper.IMemberMap> PMembers { get; }
        void Map(Dictionary<string, global::Dapper.SqlMapper.IMemberMap> mapDict);
    }

	public class TypeMap : global::Dapper.SqlMapper.ITypeMap, IMembersTypeMap
	{
		private readonly Dictionary<string, global::Dapper.SqlMapper.IMemberMap> members
			= new Dictionary<string, global::Dapper.SqlMapper.IMemberMap>(StringComparer.OrdinalIgnoreCase);
		public Type Type => type;
		private readonly Type type;
		private readonly global::Dapper.SqlMapper.ITypeMap tail;

		public void Map(string columnName, string memberName)
		{
			members[columnName] = new MemberMap(type.GetMember(memberName).Single(), columnName);
		}

		public TypeMap(Type type, global::Dapper.SqlMapper.ITypeMap tail)
		{
			this.type = type;
			this.tail = tail;
		}

		public ConstructorInfo FindConstructor( string[] names
		                                      , Type[] types)
			=> tail.FindConstructor(names, types);

		public ConstructorInfo FindExplicitConstructor()
			=> tail.FindExplicitConstructor();

		public global::Dapper.SqlMapper.IMemberMap GetConstructorParameter( ConstructorInfo constructor
														   , string columnName) 
			=> tail.GetConstructorParameter(constructor, columnName);

		public global::Dapper.SqlMapper.IMemberMap GetMember(string columnName)
		{
			global::Dapper.SqlMapper.IMemberMap map;
			if (!members.TryGetValue(columnName, out map))
			{ // you might want to return null if you prefer not to fallback to the
			  // default implementation
				map = tail.GetMember(columnName);
			}
			return map;
		}

	    public Dictionary<string, global::Dapper.SqlMapper.IMemberMap> PMembers => this.members;

	    public void Map(Dictionary<string, global::Dapper.SqlMapper.IMemberMap> mapDict)
	    {
	        foreach (var pair in mapDict)
	        {
	            this.Map(pair.Value.ColumnName, pair.Value.Property.Name);
	        }
        }
	}
}
