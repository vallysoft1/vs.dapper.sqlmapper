﻿using System;
using System.Linq.Expressions;
using VS.Extensions.BCL;

namespace VS.Dapper.SqlMapper
{
	public abstract class BaseMap
	{
		internal abstract void Mapping();
	}

	public abstract class BaseMap<T> : BaseMap
	{
		private TypeMap map;

		protected abstract void MappingPart();

		protected void Map( Expression<Func<T,object>> field, string columnName)
		{
			this.Map(field.GetPropertyName(), columnName );
		}

		protected void Map(string fieldName, string columnName )
		{
			this.map.Map(columnName, fieldName);
		}

	    protected void UseParentMap(Type type)
	    {
	        var oldMap = global::Dapper.SqlMapper.GetTypeMap(type);
	        var extMap = (IMembersTypeMap) oldMap;

	        this.map.Map(extMap.PMembers);
        }

        internal override void Mapping()
		{
		    var oldMap = global::Dapper.SqlMapper.GetTypeMap(typeof(T));
		    map = new TypeMap(typeof(T), oldMap);

            this.MappingPart();

			global::Dapper.SqlMapper.SetTypeMap(map.Type, map);
		}
	}
}