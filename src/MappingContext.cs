﻿using System;
using System.Linq;

namespace VS.Dapper.SqlMapper
{
	public static class MappingContext
	{
		public static void LoadMaps<T>()
			=> typeof(T).Assembly.GetTypes()
			            .Where(t => t.IsSubclassOf(typeof(BaseMap))
			                        && t.IsAbstract == false)
			            .Select(Activator.CreateInstance)
			            .Cast<BaseMap>()
			            .ToList()
			            .ForEach(map => map.Mapping());
	}
}