﻿using System;
using System.Reflection;

namespace VS.Dapper.SqlMapper
{
	public class MemberMap : global::Dapper.SqlMapper.IMemberMap
	{
		private readonly MemberInfo member;
		private readonly string columnName;
		public MemberMap(MemberInfo member, string columnName)
		{
			this.member = member;
			this.columnName = columnName;
		}
		public string ColumnName => columnName;
		public FieldInfo Field => member as FieldInfo;

		public Type MemberType
		{
			get
			{
				switch (member.MemberType)
				{
					case MemberTypes.Field:    return ((FieldInfo)member).FieldType;
					case MemberTypes.Property: return ((PropertyInfo)member).PropertyType;
					default:                   throw new NotSupportedException();
				}
			}
		}
		public ParameterInfo Parameter => null;
		public PropertyInfo Property => member as PropertyInfo;
	}
}
